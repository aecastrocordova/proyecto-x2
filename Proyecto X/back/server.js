var express=require('express');
var userFile=require('./user.json');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app=express();
app.use(bodyParser.json());
var totalUsers = 0;
const URL_BASE='/apitechu/v1/';
const PORT = process.env.PORT || 3000;
const user_controller = require('./controllers/user_controller');

//Petición GET de todos los 'users' (Collections)
app.get(URL_BASE + 'users',
          function(req,res){
            console.log('GET' + URL_BASE + 'users');
            res.send(userFile);
});

//Petición GET de todos los 'users' (Instance)
app.get(URL_BASE + 'users/:id',
           function(req,res){
             console.log('GET' + URL_BASE + 'users/id');
             let indice = req.params.id;
             let instancia = userFile[indice - 1];
             let respuesta=(instancia != undefined) ? instancia:{"mensaje":"Recurso no encontrado"};
             res.status(200);//
             res.send(respuesta);
});

// Peticion GET con Query String

app.get(URL_BASE + 'usersq',
           function(req,res){
             console.log('GET' + URL_BASE + 'con query String');
             //console.log(req.query);
             let respuesta=req.query;
             res.send(respuesta);
});

// Peticion POST
app.post(URL_BASE + 'users',
           function(req,res){
             totalUsers = userFile.length + 1;
             cuerpo = req.body;
             if(cuerpo!=undefined){
             let newUser={
               userId : totalUsers,
               firstName : req.body.first_name,
               lastName : req.body.last_name,
               email : req.body.email,
               password : req.body.password
               }
               userFile.push(newUser);
               res.status(200);
               res.send(
               {
                 "mensaje" : "Usuario creado con éxito",
                 "usuario": newUser,
                 "usuarios" : userFile
             });
           }else{
              res.status(404);
              res.send(
                {
                 "mensaje" : "Body vacío"
             });
           }
});

app.put(URL_BASE + 'users/:id',
           function(req,res){
               let instancia = userFile[req.params.id - 1];
               let respuesta=(instancia != undefined) ? {"mensaje":"Usuario modificado con éxito"}:{"mensaje":"Recurso no encontrado"};
               if(instancia!=undefined){
                    userFile[req.params.id - 1].first_name=req.body.first_name,
                    userFile[req.params.id - 1].last_name=req.body.last_name,
                    userFile[req.params.id - 1].email=req.body.email,
                    userFile[req.params.id - 1].password=req.body.password
                 res.status(201);
                 res.send(
                 {
                   "mensaje" : respuesta,
                   "usuario": instancia,
                   "usuarios" : userFile
                 });
                }else{
                 res.status(404);
                 res.send(respuesta);
               }
});

app.delete(URL_BASE + 'users/:id',
           function(req,res){
               let indice= req.params.id - 1;
               let instancia = userFile[indice];
               let respuesta=(instancia != undefined) ? {"mensaje":"Usuario eliminado con éxito"}:{"mensaje":"Recurso no encontrado"};
               if(instancia != undefined){
                    userFile.splice(indice,1);
                 res.status(200);
                 res.send(
                 {
                   "mensaje" : respuesta,
                   "usuario": instancia,
                   "usuarios" : userFile
                 });
                }else{
                 res.status(404);
                 res.send(respuesta);
               }
});

// Peticion POST LOGIN
app.post(URL_BASE + 'login',
           function(req,res){
             cuerpo = req.body;
             if (cuerpo != undefined){
               var user=req.body.email;
               var pass=req.body.password;
               var flag = false;

             for(var us of userFile){
               if(us.email == user){
                 if(us.password == pass){
                   us.logged = true;
                   flag=true;
                   writeUserDataToFile(userFile);
                   res.status(200);
                   res.send(
                   {
                     "mensaje" : "Login exitoso"
                   });
                    break;
                 }
               }
             }
             if(!flag){
                 res.status(400);
                 res.send(
                 {
                   "mensaje" : "Alguno de los datos ingresado es incorrecto. Verifique e intente nuevamente."
                 });
             }
         }else{
           res.status(404);
           res.send(
           {
             "mensaje" : "Body vacío"
           });
         }
});

// Peticion POST LOGOUT
app.post(URL_BASE + 'logout',
           function(req,res){
             cuerpo = req.body;
             if (cuerpo != undefined){
               var user=req.body.email;
               var flag = false;
             for(var us of userFile){
               if(us.email == user){
                 if(us.logged != undefined){
                   delete us.logged;
                   flag=true;
                   writeUserDataToFile(userFile);
                   res.status(200);
                   res.send(
                   {
                     "mensaje" : "Logout exitoso"
                   });
                    break;
                 }
               }
             }
             if(!flag){
                 res.status(400);
                 res.send(
                 {
                   "mensaje" : "Usuario no autenticado"
                 });
             }
         }else{
           res.status(404);
           res.send(
           {
             "mensaje" : "Body vacío"
           });
         }
});

// Peticion POST USUARIOS LOGUEADOS en total
app.get(URL_BASE + 'userslogged',
          function(req,res){
            console.log('GET' + URL_BASE + 'userslogged');
            var cont=0;
            var userslogged =  new Array();
            for(var us of userFile){
                if(us.logged != undefined){
                  userslogged.push(us);
                  cont += 1;
                }
            }
            if(cont > 0){
              res.status(200);
              res.send(
              {
                "mensaje" : "Listado correctamente",
                "usuarios logged:" : userslogged
              });
            }else{
              res.status(200);
              res.send(
              {
                "mensaje" : "No existen usuarios autenticados"
              });
            }
});

// Peticion POST USUARIOS LOGUEADOS con querystring
app.get(URL_BASE + 'usersloggedq',
          function(req,res){
            console.log('GET' + URL_BASE + 'usersloggedq');
            var cont=0;
            var userslogged =  new Array();
            let respuesta=req.query;
            let limiteInf=new Number(respuesta.limiteInf);
            let limiteSup=new Number(respuesta.limiteSup);
            totalUsers = userFile.length;
            if(limiteInf > limiteSup || limiteInf > totalUsers || limiteSup > totalUsers){
              res.status(400);
              res.send(
              {
                "mensaje" : "Parámetros incorrectos"
              });
              return false;
            }
            for(var us of userFile){
                if(us.logged != undefined && (us.userID>=limiteInf && us.userID<=limiteSup)){
                  cont += 1;
                  userslogged.push(us);
                }
            }
            //userslogged = userslogged.slice(limiteInf-1,limiteSup);
            if(cont > 0){
              res.status(200);
              res.send(
              {
                "mensaje" : "Listado correctamente",
                "usuarios logged limitados:" : userslogged
              });
            }else{
              res.status(200);
              res.send(
              {
                "mensaje" : "No existen usuarios autenticados con los parámetros indicados"
              });
            }
});

//METODOS CON mlab

//GET users consumiendo API REST de mlab

app.get(URL_BASE + 'usersM', user_controller.getUsers);

app.get(URL_BASE + 'usersM/:id', user_controller.getUsersID);

app.post(URL_BASE + 'usersM', user_controller.setUser);

app.put(URL_BASE + 'usersM/:id', user_controller.updateUser);

app.delete(URL_BASE + 'usersM/:id', user_controller.deleteUser);

app.post(URL_BASE + 'loginM', user_controller.loginUser);

app.post(URL_BASE + 'logoutM', user_controller.logoutUser);

//Método para escribir en un archivo físico
function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

app.listen(PORT, function(){
  console.log('escuchandooooo p 3000');
});
